### Dealing with HTML

This first introduction will cover the following functions:

```javascript
    S('Coder & power').decodeHTMLEntities().s
    S('<p>Remove all <b>tags</b> please</p>').stripTags().s
    S('This is a paragraph').wrapHTML('p').s
```

For more information, please refer to the documentation: http://stringjs.com.

var S = require('string');
var chai = require('chai');
var expect = chai.expect;

// Sources
var html = require('../sources/html');
var foo = html({});

describe('Dealing with HTML', function() {
    it('should return an object', function(done) {
        expect(foo).to.be.an('object');
        done();
    });
    it('the object should have the keys: decodeHTMLEntities, stripTags, wrapHTML', function(done) {
        expect(foo.decodeHTMLEntities).to.exist;
        expect(foo.stripTags).to.exist;
        expect(foo.wrapHTML).to.exist;
        done();
    });
    it('The key stripTags should not contain HTML tags', function(done) {
        expect(S(foo.stripTags).contains('<')).to.be.false;
        expect(S(foo.stripTags).contains('>')).to.be.false;
        done()
    });
    it('The key wrapHTML should contain HTML tags', function(done) {
        expect(S(foo.wrapHTML).contains('<')).to.be.true;
        expect(S(foo.wrapHTML).contains('>')).to.be.true;
        expect(S(foo.wrapHTML).contains('</')).to.be.true;
        done();
    });
});
